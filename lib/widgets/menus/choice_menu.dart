import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:htd_app/controllers/chat_box_controller.dart';
import 'package:htd_app/controllers/port_state_controller.dart';
import 'package:htd_app/controllers/video_state_controller.dart';
import 'package:htd_app/repository/repository.dart';
import 'dart:ui';
import 'package:get/get.dart';
import 'package:htd_app/widgets/map_page.dart';

import '../first_button.dart';
import '../link_page.dart';

class ChoiceMenu extends StatefulWidget {
  const ChoiceMenu({Key? key}) : super(key: key);

  @override
  _ChoiceMenuState createState() => _ChoiceMenuState();
}

class _ChoiceMenuState extends State<ChoiceMenu> {
  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    return Container(
      padding: EdgeInsets.only(right: width * 0.03704), // 80
      width: MediaQuery
          .of(context)
          .size
          .width,
      height: MediaQuery
          .of(context)
          .size
          .height,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                SizedBox(
                  height: height * 0.2401,
                ),
                Container(
                  width: width * 0.40185, // 868
                  child: Stack(
                    children: [
                      SizedBox(
                        width: width * 0.37037, // 800037, // 800
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: width * 0.37037, // 800
                              child: Text(
                                '어떠한 안내를 원하시나요?',
                                style: TextStyle(
                                  fontFamily: 'notoSansKR',
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xff2b2b2d),
                                  fontSize: width * 0.02407,// 52
                                  letterSpacing: -1.3,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: height * 0.00625, // 24
                            ),
                            const SideButton(img: 'assets/img/ico_mug_64px.png',
                                text: '홈·테이블데코페어\n기획관 소개',),
                            const SideButton(img: 'assets/img/ico_wifi_64px.png',
                              text: '온라인 전시관',),
                            const SideButton(img: 'assets/img/ico_sofa_64px.png',
                                text: '디자인 살롱 서울',),
                            const SideButton(img: 'assets/img/ico_note_64px.png',
                                text: 'FAQ',),

                            const WebLinkButton( text: 'BRAND LIST', link: 'https://sub.hometabledeco.com/kor/onlineD-HTD/exhibit_list.asp'),
                            const WebLinkButton( text: 'DECO MAG', link: 'http://ebookpage.co.kr/down/Sample/JK/rk_1130/index.html'),
                            const WebLinkButton( text: 'ONLINE MALL', link: 'https://www.casa.co.kr/event/exhibition.aspx?viewUrl=111'),

                            FirstButton(height: height, width: width,)
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class SideButton extends StatefulWidget {
  final String img;
  final String text;

  const SideButton(
      {Key? key, required this.img, required this.text})
      : super(key: key);

  @override
  _SideButtonState createState() => _SideButtonState();
}

class _SideButtonState extends State<SideButton> {
  Color buttonColor = Colors.black.withOpacity(0.4);

  @override
  Widget build(BuildContext context) {
    ChatBoxController chatBoxController = Get.put(ChatBoxController());
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    return GetBuilder<ChatBoxController>(
        builder: (controller) {
      return Container(
        width: width * 0.37037, // 800
        margin: EdgeInsets.only(top: height * 0.014167),
        // 40
        child: GestureDetector(
            onTapDown: (TapDownDetails t) {
              setState(() {
                buttonColor = Colors.black.withOpacity(0.7);
              });
            },
            onTapUp: (TapUpDetails tapUpDetails) async {
              setState(() {
                buttonColor = Colors.black.withOpacity(0.4);
              });
              if (chatBoxController.isRenewed) {
                chatBoxController.updateRenew(false);
                Repository.sendTextReq(widget.text);
                chatBoxController.updateMenu(false);
              }
            },
            child: ClipRRect(
              borderRadius:
              BorderRadius.circular(width * 0.007407),
              child: BackdropFilter(
                filter: ImageFilter.blur(
                  sigmaX: width * 0.00462963,
                  sigmaY: width * 0.00462963,
                ),
                child: Container(
                  padding: EdgeInsets.fromLTRB(
                      width * 0.02963,  // 64
                      height * 0.0117875,
                      0,
                      height * 0.0117875),
                  // height: height * 0.04167,
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: width * 0.000462963,
                      color: Color.fromRGBO(0, 0, 0, 0.25),
                    ),
                    color: buttonColor,
                  ),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Column(
                        children: [
                          SizedBox(
                            height: width * 0.0048,
                          ),
                          SizedBox(
                            width: width * 0.02963,
                            height: width * 0.02963,
                            child: Image.asset(
                              widget.img,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(width: width * 0.02222), // 48
                      Flexible(
                        child: Container(
                          child: Text(
                            widget.text,
                            textAlign: TextAlign.left,
                            overflow: TextOverflow.visible,
                            style: TextStyle(
                              fontFamily: 'naumSquare',
                              fontWeight: FontWeight.w400,
                              color: Colors.white,
                              fontSize: width * 0.02963,
                              height: 1.3,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )
        ),
      );
    });
  }
}

class WebLinkButton extends StatefulWidget {
  final String link;
  final String text;

  const WebLinkButton(
      {Key? key, required this.text, required this.link})
      : super(key: key);

  @override
  _WebLinkButtonState createState() => _WebLinkButtonState();
}

class _WebLinkButtonState extends State<WebLinkButton> {
  Color buttonColor = Colors.black.withOpacity(0);

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    return Container(
      width: width * 0.37037, // 800
      margin: EdgeInsets.only(top: height * 0.014167),  // 40
      child: GestureDetector(
        onTapDown: (TapDownDetails t) {
          setState(() {
            buttonColor = Colors.black.withOpacity(0);
          });
        },
        onTapUp: (TapUpDetails tapUpDetails) async {
          setState(() {
            buttonColor = Colors.black.withOpacity(0);
          });

          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => LinkPage(linkUrl: widget.link)),
          );

          Repository.sendTextReq(widget.text);
        },
        child: Container(
            padding: EdgeInsets.fromLTRB(
                width * 0.02963,  // 64
                height * 0.0117875,
                width * 0.02963,
                height * 0.0117875),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(width * 0.007407),
              border: Border.all(
                width: width * 0.000462963,
                color: Colors.black,
              ),
              color: buttonColor,
            ),
            child: Column(
                children: [
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        child: Text(
                          widget.text,
                          textAlign: TextAlign.left,
                          overflow: TextOverflow.visible,
                          style: TextStyle(
                            fontFamily: 'naumSquare',
                            fontWeight: FontWeight.w300,
                            // color: Color(0xff2b2b2d),
                            color: Color(0xff2b2b2d),
                            fontSize: width * 0.01944,  // 42
                            height: 1.3,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '바로가기',
                        textAlign: TextAlign.left,
                        overflow: TextOverflow.visible,
                        style: TextStyle(
                          fontFamily: 'naumSquare',
                          fontWeight: FontWeight.w400,
                          color: Color(0xff2b2b2d),
                          fontSize: width * 0.02407,  // 52
                          height: 1.35,
                        ),
                      ),
                      SizedBox(
                        width: width * 0.0287037, // 64
                        height: width * 0.0287037, // 64
                        child: Image.asset(
                          'assets/img/ico_arrow_right_64px.png',
                          color: Color(0xff2b2b2d),
                        ),
                      ),
                    ],
                  ),
                ],
              )
          ),
      ),
    );
  }
}

