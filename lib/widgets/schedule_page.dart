import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class SchedulePage extends StatefulWidget {
  const SchedulePage({Key? key}) : super(key: key);

  @override
  _SchedulePage createState() => _SchedulePage();
}

class _SchedulePage extends State<SchedulePage> {
  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: width,
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/img/kiosk-background.png"),
                fit: BoxFit.cover,
              ),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset(
                  "assets/img/img_timetable01.png",
                  width: width * 0.7962963, //1720
                ),
              ],
            ),
          ),
          Positioned(
            top: height * 0.195052083,  // 749
            right: width * 0.05556,  // 120
            child: SizedBox(
              width: width * 0.0444, // 96
              height: width * 0.0444, // 96
              child: InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: SizedBox(
                  child: SvgPicture.asset(
                    "assets/img/ico_close_96px.svg",
                    width:width * 0.0444,
                    height:width * 0.0444,
                  ),
                )
              ),
            ),
          ),
        ],
      ),
    );
  }
}
