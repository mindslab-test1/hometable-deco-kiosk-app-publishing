import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class MapPage extends StatefulWidget {
  const MapPage({Key? key}) : super(key: key);

  @override
  _MapPage createState() => _MapPage();
}

class _MapPage extends State<MapPage> {
  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: width,
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/img/kiosk-background.png"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Positioned(
            top: height * 0.24583,  // 944
            right: width * 0.05556, // 120
            child: SizedBox(
              child: Image.asset(
                "assets/img/img_mapA_brand.gif",
                width: width * 0.8889, //1920
              ),
            ),
          ),
          Positioned(
            top: height * 0.20833,  // 800
            right: width * 0.05556,  // 120
            child: SizedBox(
              width: width * 0.0444, // 96
              height: width * 0.0444, // 96
              child: InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: SizedBox(
                    child: SvgPicture.asset(
                      "assets/img/ico_close_96px.svg",
                      width:width * 0.0444,
                      height:width * 0.0444,
                    ),
                  )
              ),
            ),
          ),
        ],
      ),
    );
  }
}
