import 'package:get/get.dart';
import 'dart:io';

class NetworkStateController extends GetxController {
  bool _isError = false;
  bool get isError => _isError;

  void checkNetwork() async {
    try {
      await InternetAddress.lookup('maum.ai');
    } on SocketException catch (_) {
        _isError = true;
        update();
    }
  }
}
