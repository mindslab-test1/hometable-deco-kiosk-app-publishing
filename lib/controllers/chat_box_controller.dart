import 'package:get/get.dart';

class ChatBoxController extends GetxController {
  List<String> chats = [''];
  String chatTitle = '';
  String imageUrl= '';
  bool _isRenewed = true;
  bool isChatMenu = true;

  bool get isRenewed => _isRenewed;

  void updateMenu(bool isUpdate){
    isChatMenu = isUpdate;
    update();
  }

  void updateRenew(bool renew){
    _isRenewed = renew;
    update();
  }

  void changeChats(List<String> newChats) {
    chats = newChats;
  }

  void changeChatInfo(String title, String image, List<String> chat){
    chatTitle = title;
    imageUrl = image;
    chats = chat;
    update();
  }
}