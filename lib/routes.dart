import 'package:get/get.dart';
import 'package:htd_app/screens/splash_screen.dart';

final List<GetPage> routes = [
  GetPage<SplashScreen>(
      name: "/SplashScreen", page: () => const SplashScreen()),
];
