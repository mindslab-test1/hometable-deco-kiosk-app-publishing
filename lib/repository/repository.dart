import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:htd_app/controllers/chat_box_controller.dart';
import 'package:htd_app/controllers/video_state_controller.dart';

class Repository {
  static Future<void> sendTextReq(String answer) async{
    final String response = await rootBundle.loadString('assets/response.json');
    Future<String> readJson(String answer) async {
      var data = json.decode(response);
      return data[answer];
    }
    ChatBoxController chatBoxController = Get.put(ChatBoxController());
    Message message = Message.fromJson(jsonDecode((await readJson(answer)).toString().replaceAll("\\\"", "\"").replaceAll("\"{", "{").replaceAll("}\"", "}")));
    if(message.answer != null) {
      chatBoxController.changeChatInfo(
            message.answer!, message.image ?? '', message.expectedIntents ?? []);
      if(message.url != null){
        Get.put(VideoStateController()).changeUrl(message.url!, isChange: true, changingUrl: ["assets/video/wait_left.mp4"]);
      }
    }
    chatBoxController.updateRenew(true);
  }
}

class Message {
  final String? answer;
  final String? image;
  final String? url;
  final List<String>? expectedIntents;

  Message({
    this.answer,
    this.image,
    this.url,
    this.expectedIntents
  });

  Message.fromJson(dynamic json)
      : answer = json['answer']['answer']['answer'].toString(),
        image = json['answer']['answer']['image'].toString(),
        url = json['answer']['answer']['url'].toString(),
        expectedIntents = json['expectedIntents'] == null ? null : List<String>.from(json['expectedIntents'].map((item) => item['intent'].toString()).toList());
}